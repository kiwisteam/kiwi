/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiwi;

import java.util.ArrayList;
import java.util.List;
import kiwiproject.entity.HelperFollowers;
import kiwiproject.entity.Users;
import kiwiproject.services.UsersServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author inftel15
 */
@Component
public class LoginBean {

    @Autowired
    private UsersServiceImp usersService;
    
    @Autowired
    private CommentBean commentBean;

    private String email;
    private String firstName;
    private String lastName;
    private String photo;
    private Users usuarioLogueado;
    private String token;

    public LoginBean() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Users getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(Users usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String login() {
        List<HelperFollowers> lista = new ArrayList<>();
        List<HelperFollowers> lista1 = new ArrayList<>();
        List<String> lista2 = new ArrayList<>();
        
        usuarioLogueado = usersService.findByEmail(email);
        if (usuarioLogueado == null) {
            Users usuario = new Users();
            usuario.setEmail(email);
            usuario.setFirstname(firstName);
            usuario.setLastname(lastName);
            usuario.setPhoto(photo);
            usuario.setType("usuario");
            usuario.setGruposUser(lista2);
            usuario.setFollowers(lista);
            usuario.setFollowings(lista1);
            usersService.saveUsuario(usuario);
            usuarioLogueado = usuario;
        }
        commentBean.cargarComment("privado");
        return "index-fanny";
    }
    
    public void refrescarUsuario(){
        this.usuarioLogueado=usersService.findByEmail(email);
    }
    
    public String cargarUsuarioLogueado(){
        commentBean.cargarComment("privado");
        return "index-fanny";
    }

}
