/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiwi;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import kiwiproject.entity.HelperFollowers;
import kiwiproject.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import kiwiproject.services.UsersServiceImp;

@Component
public class UserBean {

    private Users user;
    private String userABuscar;
    private String userSelected;

    @Autowired
    private LoginBean loginBean;
    
    @Autowired
    private CommentBean commentBean;

    @Autowired
    private UsersServiceImp usersService;

    public UserBean() {
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getUserABuscar() {
        return userABuscar;
    }

    public void setUserABuscar(String userABuscar) {
        this.userABuscar = userABuscar;
    }

    public String getUserSelected() {
        return userSelected;
    }

    public void setUserSelected(String userSelected) {
        this.userSelected = userSelected;
    }

    @PostConstruct
    public void init() {

     //this.users=usersService.findAll();
    }

    public Users buscarUser(String email) {

        Users u = usersService.findByEmail(email);
        return u;
    }

    public List<String> buscarGroupsUser() {

        Users u = usersService.findByEmail(loginBean.getEmail());
        return u.getGroupsUser();
    }

    public String cerrarSesion() {

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        commentBean.setVista("privado");
        return "login.xhtml?faces-redirect=true";
    }

    public String irAPanelUserBuscador() {

        Users buscarUser = usersService.findByEmail(userABuscar);
        String returnValue = null;
        if (buscarUser != null) {
            this.user = buscarUser;
            commentBean.cargarCommentsPublicos(user.getEmail());
            returnValue = "userFound";
        }
        return returnValue;
        
    }
    
    public String irAPanelUser(String email){
        Users useerSelected = usersService.findByEmail(email);
        this.user = useerSelected;
        commentBean.cargarCommentsPublicos(user.getEmail());
        return "userFound";
        
    }
    
    public String irASeguidor(){
        Users seguidor = usersService.findByEmail(userSelected);
        this.user = seguidor;
        commentBean.cargarCommentsPublicos(user.getEmail());
        return "userFound";
    }
    
    public String seguir(){
        
        Users usuariologueado = loginBean.getUsuarioLogueado();
        HelperFollowers helper = new HelperFollowers();
        helper.setFirstName(this.user.getFirstname());
        helper.setLastName(this.user.getLastname());
        helper.setEmail(this.user.getEmail());
        
        if(usuariologueado.getFollowings() == null){
            List<HelperFollowers> followings = new ArrayList<>();
            followings.add(helper);
            usuariologueado.setFollowings(followings);
        }
        else{
            usuariologueado.getFollowings().add(helper);
        }
           usersService.saveUsuario(usuariologueado);
        
        //---------------------------------------------------------------
        
        HelperFollowers helper2 = new HelperFollowers();
        helper2.setFirstName(usuariologueado.getFirstname());
        helper2.setLastName(usuariologueado.getLastname());
        helper2.setEmail(usuariologueado.getEmail());
        
        if(this.user.getFollowers()==null){
            List<HelperFollowers> followers = new ArrayList<>();
            followers.add(helper2);
            this.user.setFollowers(followers);
        }
        else{
            user.getFollowers().add(helper2);
        }
        usersService.saveUsuario(user);
        
        return null;
    }
    
    public String dejarDeSeguir(){
        
        Users usuariologueado = loginBean.getUsuarioLogueado();
        for(HelperFollowers hf : usuariologueado.getFollowings()){
            if(hf.getEmail().equals(this.user.getEmail())){
                usuariologueado.getFollowings().remove(hf);
                break;
            }
        }
        usersService.saveUsuario(usuariologueado);
        
        //---------------------------------------------------------------
        
        for(HelperFollowers hf : this.user.getFollowers()){
            if(hf.getEmail().equals(usuariologueado.getEmail())){
                this.user.getFollowers().remove(hf);
                break;
            }
        }
        usersService.saveUsuario(user);
        
        return null;
    }
    
    public boolean loSigo(){
        Users usuariologueado = loginBean.getUsuarioLogueado();
        for(HelperFollowers hf : usuariologueado.getFollowings()){
            if(hf.getEmail().equals(this.user.getEmail())){
                return true;
            }
        }
        return false;
    }
    
    public List<HelperFollowers> findFollowings(){
        List<HelperFollowers> followings = loginBean.getUsuarioLogueado().getFollowings();
        return followings;
    }

}
