/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiwi;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

@SessionScoped
@ManagedBean(name ="LanguageBean")
public class LanguageBean implements Serializable {

    //private static final long serialVersionUID = 1L;

    private String locale;
    private static Map<String, Object> countries;

    public Map<String, Object> getCountries() {
        if (this.getLocale() == null) {
            this.setLocale("es");
        }

        // Actualiza el lenguaje local
        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(this.getLocale()));
        countries = new LinkedHashMap<>();

        if (this.getLocale().equals("en")) {
            countries.put("English", Locale.ENGLISH);
            countries.put("Español", new Locale("es"));
        } else {
            countries.put("Español", new Locale("es"));
            countries.put("English", Locale.ENGLISH);
        }

        return countries;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    //value change event listener
    public void localeChanged(ValueChangeEvent e) {

        String newLocaleValue = e.getNewValue().toString();

        for (Map.Entry<String, Object> entry : countries.entrySet()) {

            if (entry.getValue().toString().equals(newLocaleValue)) {

                FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) entry.getValue());
                
                this.setLocale(entry.getValue().toString());

            }
        }
    }

    public LanguageBean() {

    }

}
