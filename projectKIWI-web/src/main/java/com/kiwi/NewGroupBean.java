/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiwi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kiwiproject.entity.Group;
import kiwiproject.entity.HelperFollowers;
import kiwiproject.entity.Users;
import kiwiproject.services.GroupServiceImp;
import kiwiproject.services.UsersServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Blackproxy
 */
@Component
@Scope("session")
public class NewGroupBean implements Serializable {

    @Autowired
    private LoginBean loginbean;
    @Autowired
    private GroupServiceImp groupService;
    @Autowired
    private UsersServiceImp userService;

    private List<String> candidatesSelected;
    private List<String> membersSelected;
    private List<HelperFollowers> candidatesAvailable;
    private List<HelperFollowers> membersAvailable;
    private List<HelperFollowers> amigos;
    private String nameFilter;
    private String name;
    private String errorNombre;

    public String getErrorNombre() {
        return errorNombre;
    }

    public void setErrorNombre(String errorNombre) {
        this.errorNombre = errorNombre;
    }

    public List<HelperFollowers> getCandidatesAvailable() {
        return candidatesAvailable;
    }

    public void setCandidatesAvailable(List<HelperFollowers> candidatesAvailable) {
        this.candidatesAvailable = candidatesAvailable;
    }

    public List<HelperFollowers> getMembersAvailable() {
        return membersAvailable;
    }

    public void setMembersAvailable(List<HelperFollowers> membersAvailable) {
        this.membersAvailable = membersAvailable;
    }

    public List<HelperFollowers> getAmigos() {
        return amigos;
    }

    public void setAmigos(List<HelperFollowers> amigos) {
        this.amigos = amigos;
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public List<String> getCandidatesSelected() {
        return candidatesSelected;
    }

    public void setCandidatesSelected(List<String> candidatesSelected) {
        this.candidatesSelected = candidatesSelected;
    }

    public List<String> getMembersSelected() {
        return membersSelected;
    }

    public void setMembersSelected(List<String> membersSelected) {
        this.membersSelected = membersSelected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String createGroup() {

        Group newGroup = groupService.findByName(name);
        if (newGroup != null) {
            errorNombre="El nombre de grupo ya existe";
            return "createGroup.xhtml";
        } else {
            newGroup = new Group();
            Users usuarioLogueado = loginbean.getUsuarioLogueado();
            HelperFollowers hf = new HelperFollowers();
            hf.setEmail(usuarioLogueado.getEmail());
            hf.setFirstName(usuarioLogueado.getFirstname());
            hf.setLastName(usuarioLogueado.getLastname());
            newGroup.setName(name);
            membersAvailable.add(hf);
            newGroup.setMembers(membersAvailable);
            newGroup.setOwner(hf);
            Users usuario;
            for (HelperFollowers newMember : membersAvailable) {
                usuario = userService.findByEmail(newMember.getEmail());
                usuario.getGroupsUser().add(name);
                userService.saveUsuario(usuario);
            }
            
            groupService.createGroup(newGroup);
            loginbean.refrescarUsuario();
            return "index-fanny.xhtml?faces-redirect=true";

        }
    }

    public void leftToRight() {
        List<HelperFollowers> listNewUsers = this.emailsToUsers(candidatesSelected, candidatesAvailable);
        candidatesAvailable.removeAll(listNewUsers);
        amigos.removeAll(listNewUsers);
        membersAvailable.addAll(listNewUsers);
        candidatesSelected = null;
    }

    private List<HelperFollowers> emailsToUsers(List<String> listEmails, List<HelperFollowers> listSource) {
        List<HelperFollowers> listUsers = new ArrayList<>();
        if (listEmails != null && listSource != null) {
            for (String str : listEmails) {
                for (HelperFollowers source : listSource) {
                    if (source.getEmail().equals(str)) {
                        listUsers.add(source);
                        break;
                    }
                }
            }
        }
        return listUsers;
    }

    public void rightToLeft() {
        List<HelperFollowers> listNewUsers = this.emailsToUsers(membersSelected, membersAvailable);
        membersAvailable.removeAll(listNewUsers);
        amigos.addAll(listNewUsers);
        candidatesAvailable.addAll(listNewUsers);
        membersSelected = null;
    }
//    public void filterUsers(){
//        List <Users> listFilteredUsers = userService.findUsersByName(nameFilter);
//        candidatesAvailable = listFilteredUsers;
//        candidatesAvailable.removeAll(membersAvailable);
//        candidatesSelected=null;
//    }

    public void filterUsers() {
        candidatesAvailable = null;
        candidatesAvailable = new ArrayList<>();
        for (HelperFollowers amigo : amigos) {
            if (amigo.getFirstName().contains(nameFilter)) {
                candidatesAvailable.add(amigo);
            }
        }
    }

    public String init() {
        name ="";
        errorNombre="";
        amigos = loginbean.getUsuarioLogueado().getFollowings();
        membersAvailable = new ArrayList<>();
        nameFilter = "";
        filterUsers();
        return "createGroup.xhtml";
    }
}
