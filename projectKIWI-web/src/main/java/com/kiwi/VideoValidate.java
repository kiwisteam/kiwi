package com.kiwi;


import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author inftel17
 */
@FacesValidator("com.kiwi.VideoValidate")
public class VideoValidate implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        if (value != null) {
            String video = value.toString();
            if (!video.isEmpty()) {
                // Verificamos que es un video de youtube
                String pattern = "https?:\\/\\/(?:[0-9A-Z-]+\\.)?(?:youtu\\.be\\/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:['\"][^<>]*>|<\\/a>))[?=&+%\\w]*";

                Pattern compiledPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
                Matcher matcher = compiledPattern.matcher(video);
                if (!matcher.find()) {
                    ResourceBundle bundle = ResourceBundle.getBundle("com.kiwi.language.internacionalizacion", new Locale(FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage()));
                    FacesMessage sms = new FacesMessage(bundle.getString("NotVideoFromYoutube"));
                    sms.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(sms);

                }
            }
        }
    }
}
