/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiwi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.faces.context.FacesContext;
import kiwiproject.entity.Comment;
import kiwiproject.entity.HelperEmail;
import kiwiproject.entity.HelperFollowers;
import kiwiproject.services.CommentServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentBean {

    @Autowired
    private CommentServiceImp commentService;
    
    @Autowired
    private LoginBean loginBean;

    private String description;
    private String photo;
    private String video;
    private String tipo;
    private String userPhoto;
    private List<Comment> comments;
    private String vista = "privado"; 
    private Comment commentRemove;
    private Comment commentRekiwited;
    private String reKiwitearIn;
    private List<HelperEmail> listHelperEmail;

    public CommentBean() {
    }


    public String doComment() {

        String tipoSelect = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("groupselect");
        if (tipoSelect != null && !tipoSelect.isEmpty()) {
            vista = tipoSelect;
            if (!tipoSelect.equals("privado")) {
                comments = commentService.findCommentsByType(tipoSelect);
            } else {
                comments = commentService.findCommentsByUserAndType(loginBean.getEmail(), "privado");
            }
        }
        Collections.reverse(comments);
        return "index-fanny";
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public String getVista() {
        return vista;
    }

    public void setVista(String vista) {
        this.vista = vista;
    }

    public String getReKiwitearIn() {
        return reKiwitearIn;
    }

    public void setReKiwitearIn(String reKiwitearIn) {
        this.reKiwitearIn = reKiwitearIn;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Comment getCommentRemove() {
        return commentRemove;
    }

    public void setCommentRemove(Comment commentRemove) {
        this.commentRemove = commentRemove;
    }

    public Comment getCommentRekiwited() {
        return commentRekiwited;
    }

    public void setCommentRekiwited(Comment commentRekiwited) {
        this.commentRekiwited = commentRekiwited;
    }
    

    public String createComment() {
        Comment comment = new Comment();
        comment.setDescription(description);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");	
        Calendar cal = Calendar.getInstance();
        comment.setFecha(sdf.format(cal.getTime()));
        if (!photo.isEmpty()) {
            comment.setPhoto(photo);
        }
        comment.setTipo(this.tipo);
        comment.setOwnerEmail(loginBean.getEmail());
        HelperEmail helperEmail = new HelperEmail();
        helperEmail.setEmail(loginBean.getEmail());
        listHelperEmail = new ArrayList<>();
        listHelperEmail.add(helperEmail);
        comment.setUsersEmail(listHelperEmail);
        comment.setDescription(description);
        
        comment.setUsername(loginBean.getFirstName());
        comment.setUserPhoto(loginBean.getPhoto());
        if (!video.isEmpty()) {
            String[] partes = this.video.split("=");
            comment.setVideo(partes[1]);
        }

        commentService.insertComment(comment);
        this.description = "";
        this.photo = "";
        this.video = "";
        cargarComment(vista);
        return "index-fanny.xhtml?faces-redirect=true";

    }
    
    public void createCommentKiwi() {
        Comment comment = new Comment();
        comment.setDescription(commentRekiwited.getDescription());
        String fechanueva = commentRekiwited.getFecha();
        comment.setFecha(fechanueva);
        if (commentRekiwited.getPhoto()!=null && !commentRekiwited.getPhoto().isEmpty()) {
            comment.setPhoto(commentRekiwited.getPhoto());
        }
        comment.setTipo(reKiwitearIn);
        comment.setOwnerEmail(loginBean.getEmail());
        comment.setUsername(commentRekiwited.getUsername());
        comment.setUserPhoto(commentRekiwited.getUserPhoto());
        if (commentRekiwited.getVideo()!=null &&!commentRekiwited.getVideo().isEmpty()) {
            comment.setVideo(commentRekiwited.getVideo());
        }
        comment.setUsersEmail(commentRekiwited.getUsersEmail());

        commentService.insertComment(comment);
        this.description = "";
        this.photo = "";
        this.video = "";

    }
    
    public void rekiwitear(){
        HelperEmail helperEmail = new HelperEmail();
        helperEmail.setEmail(loginBean.getEmail());
        listHelperEmail = commentRekiwited.getUsersEmail();
        listHelperEmail.add(helperEmail);
        commentRekiwited.setUsersEmail(listHelperEmail);
        commentService.saveComment(commentRekiwited);
        createCommentKiwi();
    }
    
    public boolean rekiwited(Comment comment){
        List<Comment> listaComments = commentService.checkRekiwitit(comment.getIdComentario(), loginBean.getEmail());
        boolean estoy=!listaComments.isEmpty();
        
        return estoy;
        
    }

    public void cargarComment(String tipo) {
        if (!vista.equals("privado")) {
            comments = commentService.findCommentsByType(vista);
        } else {
            comments = commentService.findCommentsByUserAndType(loginBean.getEmail(), "privado");
        }
        Collections.reverse(comments);
    }
    
     public void borrarComentario(){
        
        commentService.removeComent(commentRemove);
        cargarComment(vista);
    }
     
     public void cargarCommentsPublicos(String email){
     
     comments=commentService.findCommentsByUserAndType(email, "publico");
     }
     
     public String tuisPublicosSeguidores(){
         
        List<String> listaEmails = new ArrayList<>();
        List<HelperFollowers> hfList = loginBean.getUsuarioLogueado().getFollowings();
        for (HelperFollowers hf : hfList){
            listaEmails.add(hf.getEmail());
        }
        List<Comment> listaComments = commentService.findCommentsFollowings(listaEmails);
        comments = listaComments;
        return null;
    }
}
