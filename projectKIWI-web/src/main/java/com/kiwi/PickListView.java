/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiwi;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
 
import org.primefaces.event.TransferEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Blackproxy
 */
@ManagedBean
public class PickListView {
     
     
    private DualListModel<String> cities;
    private DualListModel<String> themes;
     
    @PostConstruct
    public void init() {
        //Cities
        List<String> citiesSource = new ArrayList<String>();
        List<String> citiesTarget = new ArrayList<String>();
         
        citiesSource.add("San Francisco");
        citiesSource.add("London");
        citiesSource.add("Paris");
        citiesSource.add("Istanbul");
        citiesSource.add("Berlin");
        citiesSource.add("Barcelona");
        citiesSource.add("Rome");
         
        cities = new DualListModel<String>(citiesSource, citiesTarget);
         
        //Themes
        List<String> themesSource = new ArrayList<String>();
        List<String> themesTarget = new ArrayList<String>();
        
        themesSource.add("San Francisco");
        themesSource.add("London");
        themesSource.add("Paris");
        themesSource.add("Istanbul");
        themesSource.add("Berlin");
        themesSource.add("Barcelona");
        themesSource.add("Rome");
         
        themes = new DualListModel<String>(themesSource, themesTarget);
         
    }
 
    public DualListModel<String> getCities() {
        return cities;
    }
 
    public void setCities(DualListModel<String> cities) {
        this.cities = cities;
    }
 
    public DualListModel<String> getThemes() {
        return themes;
    }
 
    public void setThemes(DualListModel<String> themes) {
        this.themes = themes;
    }
     
    
 

}