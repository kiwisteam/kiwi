/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiwi;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import kiwiproject.entity.Group;
import kiwiproject.entity.HelperFollowers;
import kiwiproject.entity.Users;
import kiwiproject.services.GroupServiceImp;
import kiwiproject.services.UsersServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Blackproxy
 */
@Component
public class GroupBean {
    
    @Autowired
    private GroupServiceImp groupService;
    @Autowired
    private UsersServiceImp usersService;
    @Autowired
    private LoginBean loginbean;
    
    
    private List<HelperFollowers> members;
    private Group group;
    private String nameFilter;
    private String mensajeError;

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public String getNameFilter() {
        return nameFilter;
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
    }

    public List<HelperFollowers> getMembers() {
        return members;
    }

    public void setMembers(List<HelperFollowers> members) {
        this.members = members;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
    
    public String cargarGrupo(){
        
        String groupName = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("groupselect");
        this.group=groupService.findByName(groupName);
        
        return "groupInfo.xhtml?faces-redirect=true";
    }
    
    public String addUser(){
        Users newUser = usersService.findByEmail(nameFilter);
        boolean yaEsMiembro = false;
        List <HelperFollowers> listaMiembros = group.getMembers();
        if (newUser!=null){
            for(HelperFollowers hf:listaMiembros){
                if (hf.getEmail().equals(nameFilter)){
                    yaEsMiembro =true;
                    break;
                }
            }
            if(yaEsMiembro){
                //mensaje ya es miembro
                mensajeError="El usuario ya es miembro del grupo";
            }
            else{
                HelperFollowers nuevoMiembro = new HelperFollowers();
                nuevoMiembro.setEmail(nameFilter);
                nuevoMiembro.setFirstName(newUser.getFirstname());
                nuevoMiembro.setLastName(newUser.getLastname());
                listaMiembros.add(nuevoMiembro);
                newUser.getGroupsUser().add(group.getName());
                usersService.getRepository().save(newUser);
                groupService.getRepository().save(group);
                mensajeError="";
                nameFilter="";
            }
        }
        else{
            //mensaje no existe
            mensajeError="El usuario no existe";
        }
        return "groupInfo.xhtml?faces-redirect=true";
    }
    
    public String leaveGroup(){
        List<HelperFollowers> miembros = group.getMembers();
        int i=0;
        boolean encontrado=false;
        while(i<miembros.size() && !encontrado){
            if(miembros.get(i).getEmail().equals(loginbean.getEmail())){
                encontrado=true;
            }
            else{
                i++;
            }
        }
        miembros.remove(i);
        groupService.getRepository().save(group);
        Users yo = usersService.findByEmail(loginbean.getEmail());
        encontrado = false;
        List<String> misGrupos = yo.getGroupsUser();
        int j=0;
        while (j<misGrupos.size() && !encontrado){
            if(misGrupos.get(j).equals(group.getName())){
                encontrado=true;
            }
            else{
                j++;
            }
        }
        misGrupos.remove(j);
        usersService.getRepository().save(yo);
        
        return "index-fanny.xhtml?faces-redirect=true";
    }
    
    @PostConstruct
    public void init(){
        this.group=groupService.findByName("el grupo");
    }
    
}
