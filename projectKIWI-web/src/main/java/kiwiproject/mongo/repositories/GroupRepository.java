/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.mongo.repositories;

import kiwiproject.entity.Group;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Blackproxy
 */
@Component
@Repository
public interface GroupRepository extends MongoRepository<Group, String>{
    
    @Query("{'name': ?0 }")
    public Group findByName(String name);
}
