/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.mongo.repositories;

import java.util.List;
import kiwiproject.entity.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


@Component
@Repository
public interface CommentRepository extends MongoRepository <Comment, String> {
    @Query("{'tipo': {$regex: '?0' , $options: 'i'} }")
    public List<Comment> findCommentsByType(String tipo);
    
    @Query("{'ownerEmail': ?0 , 'tipo': {$regex: '?1' , $options: 'i'} }")
    public List<Comment> findCommentsByUserAndType(String email, String tipo);
    
    @Query("{'idComentario': ?0, 'usersEmail.email': ?1 }")
    public List<Comment> checkRekiwitit(String idComentario, String userEmail);
}
