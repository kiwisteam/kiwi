/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.entity;

import java.util.List;
import java.util.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Blackproxy
 */
@Document(collection="users")
public class Users {
    
    @Id
    private String id;
    private String email;
    private String firstname;
    private String lastname;
    private String photo;
    private String cover;
    private String type;
    private List<String> groupsUser;
    private List<HelperFollowers> followers;
    private List<HelperFollowers> followings;
    
    public Users() {}
    
    public Users(String firstName){
        this.firstname = firstName;
    }
    
    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public List<String> getGroupsUser() {
        return groupsUser;
    }

    public void setGruposUser(List<String> groupsUser) {
        this.groupsUser = groupsUser;
    }

    public List<HelperFollowers> getFollowers() {
        return followers;
    }

    public void setFollowers(List<HelperFollowers> followers) {
        this.followers = followers;
    }

    public List<HelperFollowers> getFollowings() {
        return followings;
    }

    public void setFollowings(List<HelperFollowers> followings) {
        this.followings = followings;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Users other = (Users) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.firstname, other.firstname)) {
            return false;
        }
        if (!Objects.equals(this.lastname, other.lastname)) {
            return false;
        }
        if (!Objects.equals(this.photo, other.photo)) {
            return false;
        }
        if (!Objects.equals(this.cover, other.cover)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.groupsUser, other.groupsUser)) {
            return false;
        }
        if (!Objects.equals(this.followers, other.followers)) {
            return false;
        }
        if (!Objects.equals(this.followings, other.followings)) {
            return false;
        }
        return true;
    }
    
    
    
}
