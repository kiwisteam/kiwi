/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.entity;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Blackproxy
 */
@Document(collection="group")
public class Group {
    
    @Id
    private String id;
    private String name;
    private HelperFollowers owner;
    private List<HelperFollowers> members;
    private String creationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HelperFollowers getOwner() {
        return owner;
    }

    public void setOwner(HelperFollowers owner) {
        this.owner = owner;
    }

    public List<HelperFollowers> getMembers() {
        return members;
    }

    public void setMembers(List<HelperFollowers> members) {
        this.members = members;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
    
}
