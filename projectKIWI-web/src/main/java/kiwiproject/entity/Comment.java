/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.entity;


import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author inftel15
 */
@Document(collection="comment")
public class Comment {
    @Id
    private String idComentario;
    private String userEmail;
    private String username;
    private String fecha;
    private String description;
    private String photo;
    private String video;
    private List<HelperEmail> usersEmail;
    private String tipo;
    private String userPhoto;
    private String ownerEmail;

    public Comment() {}

    public Comment(String username, String userEmail, String fecha, String description, List<HelperEmail> usersEmail, String ownerEmail, String photo, String video, String tipo,String userPhoto) {
//        this.idComentario = System.currentTimeMillis();
        this.username = username;
        this.userEmail = userEmail;
        this.fecha = fecha;
        this.description = description;
        this.usersEmail = usersEmail;
        this.photo = photo;
        this.video = video;
        this.tipo = tipo;
        this.ownerEmail = ownerEmail;
        this.userPhoto=userPhoto;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<HelperEmail> getUsersEmail() {
        return usersEmail;
    }

    public void setUsersEmail(List<HelperEmail> userEmail) {
        this.usersEmail = userEmail;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }
     public String getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(String idComentario) {
        this.idComentario = idComentario;
    }
}
