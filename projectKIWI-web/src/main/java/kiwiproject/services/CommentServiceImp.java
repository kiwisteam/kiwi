/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.services;

import java.util.Collections;
import java.util.List;
import kiwiproject.entity.Comment;
import kiwiproject.mongo.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 *
 * @author inftel15
 */
@Component
@Service
public class CommentServiceImp {

    @Autowired
    private CommentRepository repository;

    public CommentRepository getRepository() {
        return repository;
    }

    public boolean insertComment(Comment comment) {
        repository.insert(comment);
        return true;
    }

    public boolean saveComment(Comment comment) {
        repository.save(comment);
        return true;
    }
    
    public Comment find(String id) {
        return repository.findOne(id);
    }

    public List<Comment> findAllComment() {

        return repository.findAll();
    }

    public List<Comment> findCommentsByType(String tipo) {
        return repository.findCommentsByType(tipo);
    }

    public List<Comment> findCommentsByUserAndType(String email, String tipo) {
        return repository.findCommentsByUserAndType(email, tipo);
    }

    public boolean removeComent(Comment comment) {

        repository.delete(comment);
        return true;
    }

    public List<Comment> findCommentsFollowings(List<String> emails) {

        ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig.xml");
	MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
        
        Query query = new Query();
        query.addCriteria(Criteria.where("ownerEmail").in(emails).and("tipo").is("publico"));

        List<Comment> listaComments = mongoOperation.find(query, Comment.class);
        Collections.reverse(listaComments);

        return listaComments;
    }
    
    public List<Comment> checkRekiwitit(String id, String userEmail){
        return repository.checkRekiwitit(id,userEmail);
    }
}
