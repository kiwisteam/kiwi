/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.services;

import java.util.List;
import kiwiproject.entity.Users;
import kiwiproject.mongo.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Blackproxy
 */
@Service
public class UsersServiceImp {

    @Autowired
    private UsersRepository repository;

    public UsersRepository getRepository() {
        return repository;
    }

    public Users findByEmail(String email) {
//        Query a = new Query();
//        a.addCriteria(Criteria.where("email").is(email));
//        ApplicationContext ac = new GenericXmlApplicationContext("SpringConfig.xml");
//        MongoOperations mongoO = (MongoOperations) ac.getBean("mongoTemplate");
//
//        Users u = mongoO.findOne(a, Users.class);
//        return u;
        return repository.findUsersByEmail(email);
    }
    
    
    public List<Users> findUsersByName(String name){
        return repository.findUsersByName(name);
    }

    public Users saveUsuario(Users u) {
        return repository.save(u);
    }
    
    public List<Users> findAllUsers() {

        return repository.findAll();

    }
    
    public Users find(String id) {
        return repository.findOne(id);
    }
}
