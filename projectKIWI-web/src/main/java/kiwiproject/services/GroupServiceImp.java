/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.services;

import java.util.List;
import kiwiproject.entity.Group;
import kiwiproject.entity.Users;
import kiwiproject.mongo.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 *
 * @author Blackproxy
 */
@Component
@Service
public class GroupServiceImp {
    
    @Autowired
    private GroupRepository repository;
    
    public GroupRepository getRepository(){
        return repository;
    }
    
    public Group find(String id) {
        return repository.findOne(id);
    }
    
    public List<Group> findAll(){
        return repository.findAll();
    }
    
    public Group findByName(String name){
        return repository.findByName(name);
    }
    
    public Group createGroup(Group group){
        return repository.save(group);
    }
    
    public void deleteGroup(Group group){
        repository.delete(group);
    }
    
    public Group findByOwner(Users owner){
        Query query = new Query();
        query.addCriteria(Criteria.where("owners").in(owner));
        ApplicationContext ac = new GenericXmlApplicationContext("SpringConfig.xml");
        MongoOperations mongoOperations = (MongoOperations) ac.getBean("mongoTemplate");
        Group group = mongoOperations.findOne(query, Group.class);
        
        return group;
    }
    
}
