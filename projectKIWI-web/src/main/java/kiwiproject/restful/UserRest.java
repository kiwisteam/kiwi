/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.restful;

import java.util.List;
import kiwiproject.entity.HelperEmail;
import kiwiproject.entity.Users;
import kiwiproject.services.UsersServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author inftel14
 */
@RestController
@RequestMapping("/user")
public class UserRest {

    @Autowired
    private UsersServiceImp usersServiceImp;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    Users create(@RequestBody Users entity) {
        Users user = usersServiceImp.findByEmail(entity.getEmail());
        if(user == null) {
            user = usersServiceImp.saveUsuario(entity);
        }
        return user;
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void edit(@RequestBody ListWrapper<Users> listWrapper) {
        for (Users user : listWrapper.getList()) {
            usersServiceImp.saveUsuario(user);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Users find(@PathVariable("id") String id) {
        return usersServiceImp.find(id);
    }
    
    @RequestMapping(value = "/email", method = RequestMethod.POST)
    public @ResponseBody
    Users findByEmail(@RequestBody HelperEmail email) {
        return usersServiceImp.findByEmail(email.getEmail());
    }

    @RequestMapping(value = "/name={name}", method = RequestMethod.GET)
    public @ResponseBody
    List<Users> findByName(@PathVariable("name") String name) {
        return usersServiceImp.findUsersByName(name);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    List<Users> findAll() {
        return usersServiceImp.findAllUsers();
    }
}
