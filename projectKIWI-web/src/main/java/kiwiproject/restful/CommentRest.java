/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.restful;

import java.util.List;
import kiwiproject.entity.Comment;
import kiwiproject.entity.Users;
import kiwiproject.services.CommentServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author inftel16
 */
@RestController
@RequestMapping("/comment")
public class CommentRest {
    
    @Autowired
    private CommentServiceImp commentServiceImp;
    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void create(@RequestBody Comment comment) {
        commentServiceImp.insertComment(comment);
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void edit(@RequestBody ListWrapper<Comment> listWrapper) {
        for (Comment comment : listWrapper.getList()) {
            commentServiceImp.saveComment(comment);
        }
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable("id") String id) {
        Comment comment = commentServiceImp.find(id);
        commentServiceImp.removeComent(comment);
    }
    
    @RequestMapping(value = "/type={type}", method = RequestMethod.GET)
    public @ResponseBody List<Comment> findByType(@PathVariable("type") String type){
        return commentServiceImp.findCommentsByType(type);
    }
    
    @RequestMapping(value = "/email={email}&type={type}", method = RequestMethod.GET)
    public @ResponseBody List<Comment> findByType(@PathVariable("email") String email, @PathVariable("type") String type){
        return commentServiceImp.findCommentsByUserAndType(email.replace("_", "."), type);
    }
    
    @RequestMapping(value = "/followings", method = RequestMethod.GET)
    public @ResponseBody List<Comment> findByFollowings(@RequestBody List<String> emails) {
        return commentServiceImp.findCommentsFollowings(emails);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    List<Comment> findAll() {
        return commentServiceImp.findAllComment();
    }
}
