/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kiwiproject.restful;

import java.util.ArrayList;
import java.util.List;
import kiwiproject.entity.Group;
import kiwiproject.entity.HelperFollowers;
import kiwiproject.entity.Users;
import kiwiproject.services.GroupServiceImp;
import kiwiproject.services.UsersServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author inftel16
 */
@RestController
@RequestMapping("/group")
public class GroupRest {

    @Autowired
    private GroupServiceImp groupServiceImp;
    @Autowired
    private UsersServiceImp usersServiceImp;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    Group create(@RequestBody Group entity) {
        Group group = groupServiceImp.findByName(entity.getName());
        if (group != null && (group.getId() == null || group.getId().isEmpty())) {
            return entity;
        }
        List<HelperFollowers> members = entity.getMembers();
        for (HelperFollowers member : members) {
            Users user = usersServiceImp.findByEmail(member.getEmail());
            if (user.getGroupsUser() == null) {
                user.setGruposUser(new ArrayList<String>());
            }
            if (!user.getGroupsUser().contains(entity.getName())) {
                user.getGroupsUser().add(entity.getName());
            }
            usersServiceImp.saveUsuario(user);
        }
        return groupServiceImp.createGroup(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable("id") String id) {
        Group group = groupServiceImp.find(id);
        List<HelperFollowers> members = group.getMembers();
        for (HelperFollowers member : members) {
            Users user = usersServiceImp.findByEmail(member.getEmail());
            if (user.getGroupsUser().contains(group.getName())) {
                user.getGroupsUser().remove(group.getName());
            }
            usersServiceImp.saveUsuario(user);
        }
        groupServiceImp.deleteGroup(group);
    }

    @RequestMapping(value = "/owner", method = RequestMethod.GET)
    public @ResponseBody
    Group findByOwner(@RequestBody Users user) {
        return groupServiceImp.findByOwner(user);

    }

    @RequestMapping(value = "/name={name}", method = RequestMethod.GET)
    public @ResponseBody
    Group findByName(@PathVariable("name") String name) {
        return groupServiceImp.findByName(name);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    List<Group> findAll() {
        return groupServiceImp.findAll();
    }

    @RequestMapping(value = "/id={groupId}&user={userId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void leaveGroup(@PathVariable("groupId") String groupId, @PathVariable("userId") String userId) {
        Group group = groupServiceImp.find(groupId);
        Users user = usersServiceImp.find(userId);
        user.getGroupsUser().remove(group.getName());
        HelperFollowers helperFollower;
        for (int i = group.getMembers().size() - 1; i >= 0; i--) {
            helperFollower = group.getMembers().get(i);
            if (helperFollower.getEmail().compareTo(user.getEmail()) == 0) {
                group.getMembers().remove(helperFollower);
                break;
            }
        }
        usersServiceImp.saveUsuario(user);
        groupServiceImp.createGroup(group);
    }
}
