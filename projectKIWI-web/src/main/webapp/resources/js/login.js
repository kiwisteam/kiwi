var token;

(function () {
    console.log('entra aqui');
    var po = document.createElement('script');
    po.type = 'text/javascript';
    po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
})();

function signinCallback(authResult) {
    if (authResult['access_token']) {
        console.log('aqui tb');
        document.getElementById('form:token').value = authResult['access_token'];
        console.log(authResult['access_token']);
        // Autorizado correctamente
        // Oculta el botón de inicio de sesión ahora que el usuario está autorizado, por ejemplo:
        document.getElementById('signinButton').setAttribute('style', 'display: none');
        gapi.auth.setToken(authResult);
        getDatosUsuario();
    } else if (authResult['error']) {
        // Se ha producido un error.
        // Posibles códigos de error:
        //   "access_denied": el usuario ha denegado el acceso a la aplicación.
        //   "immediate_failed": no se ha podido dar acceso al usuario de forma automática.
        // console.log('There was an error: ' + authResult['error']);
    }
}

function getDatosUsuario() {
    // Carga las bibliotecas oauth2 para habilitar los métodos userinfo.
    gapi.client.load('oauth2', 'v2', function () {
        var request = gapi.client.oauth2.userinfo.get();
        request.execute(getEmailCallback);
    });
}

function getEmailCallback(obj) {
    console.log(obj);
    document.getElementById('form:email').value = obj['email'];
    document.getElementById('form:firstName').value = obj['given_name'];
    document.getElementById('form:lastName').value = obj['family_name'];
    document.getElementById('form:photo').value = obj['picture'];
    document.getElementById("form:buttonLog").click();
}

window.onbeforeunload = function (e) {
    gapi.auth.signOut();
};

